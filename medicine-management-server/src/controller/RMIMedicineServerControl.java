package controller;

import model.Medicine;
import model.Use;
import view.RMIMedicineServerView;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RMIMedicineServerControl extends UnicastRemoteObject implements RMISearchMedicineInterface {
    private int serverPort = 3232;
    private Registry registry;
    private Connection con;
    private RMIMedicineServerView view;
    private String rmiService = "rmiMedicineManagementServer";

    public RMIMedicineServerControl(RMIMedicineServerView view) throws RemoteException {
        this.view = view;
        getDBConnection("medicine_management", "root", "dhh13072001");
        view.showMessage("RMI server is running...");
        try {
            registry = LocateRegistry.createRegistry(serverPort);
            registry.rebind(rmiService, this);
        } catch (RemoteException e) {
            throw e;
        }
    }

    private void getDBConnection(String dbName, String username, String password) {
        System.out.println("Connecting to DB ...");
        String dbUrl = "jdbc:mysql://127.0.0.1:3306/" + dbName;
        try {
            con = DriverManager.getConnection(dbUrl, username, password);
        } catch (Exception e) {
            System.out.println("Connect to DB fail !!!");
            view.showMessage(e.getMessage().toString());
        }
    }

    @Override
    public List<Medicine> searchMedicine(String name) throws RemoteException {
        String orderBy = "name";
        String query = "SELECT * FROM medicines WHERE name LIKE \"%" + name + "%\" ORDER BY "+ orderBy +";";
        List<Medicine> resultListMedicine = new ArrayList<Medicine>();
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Medicine tempMedicine = new Medicine();
                tempMedicine.setId(rs.getInt("id"));
                tempMedicine.setName(rs.getString("name"));
                tempMedicine.setPrice(rs.getInt("price"));
                tempMedicine.setType(rs.getString("type"));
                resultListMedicine.add(tempMedicine);
            }
            for (Medicine medicine: resultListMedicine){
                int medicineId = medicine.getId();
                String queryUseByMedicine = "SELECT u.* FROM uses AS u JOIN medicine_use AS mu ON u.id = mu.use_id WHERE mu.medicine_id = "+ medicineId +";";
                ResultSet rsUses = stmt.executeQuery(queryUseByMedicine);
                String medicineUse = "";
                while(rsUses.next()){
                    medicineUse += rsUses.getString("name");
                    medicineUse += ",";
                }
                medicineUse = medicineUse.substring(0, medicineUse.length() - 1);
                medicine.setUse(medicineUse);
            }
            System.out.println(resultListMedicine.get(0).toString());
        } catch (SQLException e) {
            view.showMessage(e.getMessage().toString());
            throw new RuntimeException(e);
        }
        return resultListMedicine;
    }

    @Override
    public Medicine addMedicine(String name, long price, String type, String uses) throws RemoteException {
        String sql = "INSERT INTO medicines (`name`, `price`, `type`) VALUES ('" + name + "', '"+ price +"', '" + type + "');";
        String sqlSearch = "SELECT * FROM medicines WHERE name = '" + name + "'";
        String[] listUseName = uses.split(",");
        List<Integer> listUseId = new ArrayList<Integer>();
        Medicine resultMedicine = new Medicine();
        try {
            Statement stmt = con.createStatement();
            int rs = stmt.executeUpdate(sql);
            ResultSet rsSearch = stmt.executeQuery(sqlSearch);
            while (rsSearch.next()) {
                resultMedicine.setId(rsSearch.getInt("id"));
                resultMedicine.setName(rsSearch.getString("name"));
                resultMedicine.setPrice(rsSearch.getInt("price"));
                resultMedicine.setType(rsSearch.getString("type"));
            }
            for (int i = 0; i < listUseName.length; i++){
                String sqlSearchUse = "SELECT * FROM uses WHERE name = '" + listUseName[i] + "'";
                ResultSet rsAddUse = stmt.executeQuery(sqlSearchUse);
                while (rsAddUse.next()) {
                    int useId = rsAddUse.getInt("id");
                    listUseId.add(useId);
                }
            }
            for (int i = 0; i < listUseId.size(); i++){
                String sqlAddUseToMedicine = "INSERT INTO `medicine_use` (`medicine_id`, `use_id`) VALUES ('"+ resultMedicine.getId() +"', '" + listUseId.get(i) + "');";
                System.out.println(sqlAddUseToMedicine);
                int rsAddUseToMedicine = stmt.executeUpdate(sqlAddUseToMedicine);
            }
            resultMedicine.setUse(uses);
        } catch (Exception e) {
            view.showMessage(e.getMessage().toString());
            throw new RuntimeException(e);
        }
        return resultMedicine;
    }

    @Override
    public Use addUse(String name, String description) throws RemoteException {
        String sql = "INSERT INTO `medicine_management`.`uses` (`name`, `description`) VALUES ('"+ name +"', '"+ description +"');";
        try {
            Statement stmt = con.createStatement();
            int rs = stmt.executeUpdate(sql);
            Use newUse = new Use();
            String sqlSearch = "SELECT * FROM uses WHERE name = '" + name + "'";
            ResultSet rsSearch = stmt.executeQuery(sqlSearch);
            while (rsSearch.next()) {
                newUse.setId(rsSearch.getInt("id"));
                newUse.setName(rsSearch.getString("name"));
                newUse.setDescription(rsSearch.getString("description"));
            }
            return newUse;
        } catch (Exception e){
            System.out.println(e.getMessage().toString());
        }
        return null;
    }

    @Override
    public Use updateUse(int id, String name, String description) throws RemoteException {
        String sql = "UPDATE `uses` SET `name` = '" + name + "', `description` = '" + description + "' WHERE (`id` = '" + id + "');";
        try {
            Statement stmt = con.createStatement();
            int rs = stmt.executeUpdate(sql);
            Use updatedUse = new Use();
            String sqlSearch = "SELECT * FROM uses WHERE id = '" + id + "'";
            ResultSet rsSearch = stmt.executeQuery(sqlSearch);
            while (rsSearch.next()) {
                updatedUse.setId(rsSearch.getInt("id"));
                updatedUse.setName(rsSearch.getString("name"));
                updatedUse.setDescription(rsSearch.getString("description"));
            }
            return updatedUse;
        } catch (Exception e){
            System.out.println(e.getMessage().toString());
        }
        return null;
    }

    @Override
    public boolean deleteUse(int id) throws RemoteException {
        String sql = "DELETE FROM uses WHERE id = "+ id +";";
        String sqlMedicineUse = "DELETE FROM medicine_use WHERE use_id = "+ id +";";
        try {
            Statement stmt = con.createStatement();
            int rs2 = stmt.executeUpdate(sqlMedicineUse);
            int rs = stmt.executeUpdate(sql);
        } catch (Exception e){
            System.out.println(e.getMessage().toString());
            return false;
        }
        return true;
    }

    @Override
    public List<Use>  searchUses(String name) throws RemoteException {
        String orderBy = "name";
        String query = "SELECT * FROM uses WHERE name LIKE \"%" + name + "%\" ORDER BY "+ orderBy +";";
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            List<Use> resultListUse = new ArrayList<Use>();
            while (rs.next()) {
                Use tempUse = new Use();
                tempUse.setId(rs.getInt("id"));
                tempUse.setName(rs.getString("name"));
                tempUse.setDescription(rs.getString("description"));
                resultListUse.add(tempUse);
            }
            System.out.println(resultListUse);
            return resultListUse;
        } catch (Exception e){
            System.out.println(e.getMessage().toString());
        }
        return null;
    }

    @Override
    public Medicine updateMedicine(int id, String name, long price, String type, String uses) throws RemoteException {
        String[] listUseName = uses.split(",");
        List<Integer> listUseId = new ArrayList<Integer>();
        String sql = "UPDATE `medicines` SET `name` = '" + name + "', `price` = '" + price + "', `type` = '" + type + "' WHERE (`id` = '" + id + "');";
        String sqlRemoveMedicineUse = "DELETE FROM medicine_use WHERE medicine_id = "+ id +";";
        try {
            Statement stmt = con.createStatement();
            int rs = stmt.executeUpdate(sql);
            int rs2 = stmt.executeUpdate(sqlRemoveMedicineUse);
            Medicine updatedMedicine = new Medicine();
            String sqlSearch = "SELECT * FROM medicines WHERE id = '" + id + "'";
            ResultSet rsSearch = stmt.executeQuery(sqlSearch);
            while (rsSearch.next()) {
                updatedMedicine.setId(rsSearch.getInt("id"));
                updatedMedicine.setName(rsSearch.getString("name"));
                updatedMedicine.setPrice(rsSearch.getLong("price"));
                updatedMedicine.setType(rsSearch.getString("type"));
            }
            String updatedMedicineUse = "";
            for (int i = 0; i < listUseName.length; i++){
                String sqlSearchUse = "SELECT * FROM uses WHERE name = '" + listUseName[i] + "'";
                ResultSet rsAddUse = stmt.executeQuery(sqlSearchUse);
                while (rsAddUse.next()) {
                    int useId = rsAddUse.getInt("id");
                    updatedMedicineUse += rsAddUse.getString("name");
                    updatedMedicineUse += ",";
                    listUseId.add(useId);
                }
            }
            for (int i = 0; i < listUseId.size(); i++){
                String sqlAddUseToMedicine = "INSERT INTO `medicine_use` (`medicine_id`, `use_id`) VALUES ('"+ id +"', '" + listUseId.get(i) + "');";
                System.out.println(sqlAddUseToMedicine);
                int rsAddUseToMedicine = stmt.executeUpdate(sqlAddUseToMedicine);
            }
            updatedMedicineUse = updatedMedicineUse.substring(0, updatedMedicineUse.length() - 1);
            updatedMedicine.setUse(updatedMedicineUse);
            return updatedMedicine;
        } catch (Exception e){
            System.out.println(e.getMessage().toString());
        }
        return null;
    }

    @Override
    public boolean deleteMedicine(int id) throws RemoteException {
        String sql = "DELETE FROM medicines WHERE id = "+ id +";";
        String sqlMedicineUse = "DELETE FROM medicine_use WHERE medicine_id = "+ id +";";
        try {
            Statement stmt = con.createStatement();
            int rs2 = stmt.executeUpdate(sqlMedicineUse);
            int rs = stmt.executeUpdate(sql);
        } catch (Exception e){
            System.out.println(e.getMessage().toString());
            return false;
        }
        return true;
    }
}

