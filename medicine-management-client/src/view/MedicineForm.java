package view;

import model.Medicine;
import model.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class MedicineForm extends JFrame implements ActionListener {
    private JTextField txtMedicineName;
    private JButton searchMedicineButton;

    public MedicineForm() {
        JPanel content = new JPanel();
        content.setLayout(new FlowLayout());
        txtMedicineName.setPreferredSize(new Dimension(300, 30));
        content.add(txtMedicineName);
        content.add(searchMedicineButton);
        this.setContentPane(content);
        this.pack();
        this.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                System.exit(0);
            }
        });

        searchMedicineButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
    }

    public void addLoginListener(ActionListener search) {
        searchMedicineButton.addActionListener(search);
    }

    public void showMessage(String msg) {
        JOptionPane.showMessageDialog(this, msg);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    public String getMedicineName() {
        String medicineName = txtMedicineName.getText();
        return medicineName;
    }

    public void showListMedicine(List<Medicine> listMedicine){
        JFrame f = new JFrame("List Medicine");
        String[] data = new String[10000];
        for (int i=0; i < listMedicine.size(); i++){
            data[i] = listMedicine.get(i).toString();
        }
        f.add(new JList(data));
        f.pack();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }
}
