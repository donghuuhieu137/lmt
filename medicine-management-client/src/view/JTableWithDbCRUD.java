package view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

public class JTableWithDbCRUD extends JFrame implements ActionListener{
    private JToolBar toolbar;
    private JButton btnF,btnP,btnN,btnL,btnAdd,btnSave,btnDelete;
    private DefaultTableModel dtm;
    private JTable table;
    private UpdateAction action;
    private JLabel statusBar;

    private ArrayList<Lophoc> lst;
    private int curPos=0;//vị trí hiện tại của dòng trên lưới
    private JLabel lblPos;
    private int NoExistRecords=0;//Giữ số mẫu tin cũ
    public JTableWithDbCRUD() throws Exception{
        setTitle("MyDataGridViewDemo");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(600,400);
        this.add(statusBar=new JLabel("ready..."),BorderLayout.SOUTH);
        createToolbar();

        action=new UpdateAction();
        lst=action.getAllRows();

        String []title={"Mã","Tên","Tên GVCN"};
        dtm=new DefaultTableModel(title,0){
            public boolean isCellEditable(int row, int column) {
                if(column==0&&(row>=0&&row<lst.size()))
                    return false;
                return true;
            };
        };
        table=new JTable(dtm);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);//cho chọn 1 dòng
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                curPos=table.getSelectedRow();
                lblPos.setText((curPos+1)+"/"+dtm.getRowCount());
            }
        });
        this.add(new JScrollPane(table));
        FillData(lst);//điền dữ liệu lên JTable
        table.setRowSelectionInterval(curPos, curPos);
    }
    /**
     * Điền dữ liệu từ bảng lên JTable
     */
    private void FillData(ArrayList<Lophoc> lst){
        try {
            for(Lophoc lh:lst){
                String []row={lh.getMsLop(),lh.getTenLop(),lh.getTenGVCN()};
                dtm.addRow(row);
            }
            NoExistRecords=lst.size();//Số mẫu tin cũ
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createToolbar(){
        this.add(toolbar=new JToolBar(),BorderLayout.NORTH);
        toolbar.add(btnF=new JButton(new ImageIcon("images/MoveFirstHS.png")));
        toolbar.add(btnP=new JButton(new ImageIcon("images/MovePreviousHS.png")));
        toolbar.add(lblPos=new JLabel());
        toolbar.add(btnN=new JButton(new ImageIcon("images/MoveNextHS.png")));
        toolbar.add(btnL=new JButton(new ImageIcon("images/MoveLastHS.png")));
        toolbar.add(new JToolBar.Separator());
        toolbar.add(btnAdd=new JButton(new ImageIcon("images/NewDocumentHS.png")));
        toolbar.add(btnSave=new JButton(new ImageIcon("images/SaveAllHS.png")));
        toolbar.add(btnDelete=new JButton(new ImageIcon("images/deleteHS.png")));
        btnAdd.addActionListener(this);btnSave.addActionListener(this);
        btnDelete.addActionListener(this);btnF.addActionListener(this);
        btnP.addActionListener(this);btnN.addActionListener(this);
        btnL.addActionListener(this);
        btnAdd.setToolTipText("Insert new row");
        btnSave.setToolTipText("Update data to database");
        btnDelete.setToolTipText("Delete selection row");
    }

    //Thực thi chương trình
    public static void main(String[] args)throws Exception {
        new JTableWithDbCRUD().setVisible(true);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        Object o=e.getSource();
        if(o.equals(btnAdd)){
            String []row={"","",""};
            dtm.addRow(row);
            //dtm.setRowCount(dtm.getRowCount()+1);
        }
        else if(o.equals(btnDelete)){
            try{
                int selrow=table.getSelectedRow();
                String id=dtm.getValueAt(selrow, 0).toString();
                if(id.trim().equals("")){
                    dtm.removeRow(selrow);
                    statusBar.setText("delete row completed");
                }
                else if(action.DeleteLophocByID(id)){
                    dtm.removeRow(selrow);
                    statusBar.setText("delete row completed");
                }
                else
                    statusBar.setText("delete row failed");
            }
            catch (Exception e2) {
                JOptionPane.showMessageDialog(null,
                        "Select Row before using this command","Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
        else if(o.equals(btnSave)){
            try {
                xulycapnhat();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        else{
            XuLyCacNutDichuyen(e);
        }
    }
    /**
     * 1.Duyệt qua từng dòng trong JTable<br/>
     * 2.Lấy giá trị ID của mỗi dòng<br/>
     * 3.Kiểm tra ID này trong cơ sở dữ liệu<br/>
     *  <ul>
     *  <li>Nếu có
     *      <ul>
     *      <li>Kiểm tra các cột còn lại có thay đổi gì không
     *          <ul>
     *          <li>Nếu có thì tiến hành cập nhật
     *          <li>Ngược lại thì bỏ qua
     *          </ul>
     *      </ul>
     *  <li>Nếu không thì tiến hành thêm 1 mẫu tin mới
     *  </ul>
     *  Chưa xử lý được vấn đề sửa giá trị id thì xóa record cũ thêm record mới
     */
    private void xulycapnhat() throws Exception{
        int rows=dtm.getRowCount();
        //int cols=dtm.getColumnCount();

        for (int i = 0; i < rows; i++) {
            String ms=dtm.getValueAt(i, 0).toString();
            String tenlop=dtm.getValueAt(i, 1).toString();
            String gvcn=dtm.getValueAt(i, 2).toString();
            Lophoc newlh=new Lophoc(ms,tenlop,gvcn);

            //nếu i<so mau tin cu thì chỉ cho cập nhật
            if(i<NoExistRecords){
                Lophoc lh=action.getLophocByID(ms);
                if(!lh.getTenLop().equalsIgnoreCase(tenlop)
                        || !lh.getTenGVCN().equalsIgnoreCase(gvcn)){
                    action.UpdateLop(newlh);
                }
            }
            else{//Những dòng mới thêm
                //dòng không có msLop thì bỏ qua
                if(ms.trim().equals(""))continue;

                Lophoc lh=action.getLophocByID(ms);
                if(lh!=null){//mẫu tin thêm vào trùng ID
                    if(!lh.getTenLop().equalsIgnoreCase(tenlop)
                            || !lh.getTenGVCN().equalsIgnoreCase(gvcn)){
                        action.UpdateLop(newlh);
                    }
                }
                else{//Mẫu tin mới
                    action.AddNewLophoc(newlh);
                }
            }

            /*String ms=dtm.getValueAt(i, 0).toString();
            String tenlop=dtm.getValueAt(i, 1).toString();
            String gvcn=dtm.getValueAt(i, 2).toString();
            Lophoc newlh=new Lophoc(ms,tenlop,gvcn);
            Lophoc lh=action.getLophocByID(ms);
            //dòng không có msLop thì bỏ qua
            if(ms.trim().equals(""))continue;
            if(lh!=null){//mẫu tin cũ
                if(!lh.getTenLop().equalsIgnoreCase(tenlop)
                        || !lh.getTenGVCN().equalsIgnoreCase(gvcn)){
                    action.UpdateLop(newlh);
                }
            }
            else{//Mẫu tin mới
                action.AddNewLophoc(newlh);
            }*/
        }
        statusBar.setText("Update completed");
    }

    private void XuLyCacNutDichuyen(ActionEvent e) {
        Object o=e.getSource();
        if(o.equals(btnF)){
            curPos=0;
        }
        else if(o.equals(btnP)){
            if(curPos>0) curPos--;
        }
        else if(o.equals(btnN)){
            if(curPos<dtm.getRowCount()-1) curPos++;
        }
        else if(o.equals(btnL)){
            curPos=dtm.getRowCount()-1;
        }
        table.setRowSelectionInterval(curPos, curPos);
    }
}