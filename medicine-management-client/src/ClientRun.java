import controller.RMIMedicineManageControl;
import view.MedicineForm;

public class ClientRun {
    public static void main(String[] args) {
        MedicineForm medicineView = new MedicineForm();
        RMIMedicineManageControl control = new RMIMedicineManageControl(medicineView);
        medicineView.setVisible(true);
    }
}
