package controller;

import model.Medicine;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface RMISearchMedicineInterface extends Remote {
    public List<Medicine> searchMedicine(String name) throws RemoteException;
}
