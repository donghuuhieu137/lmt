package controller;

import model.Medicine;
import model.User;
import view.MedicineForm;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;

public class RMIMedicineManageControl {
    private MedicineForm view;
    private String serverHost = "localhost";
    private int serverPort = 3232;
    private RMISearchMedicineInterface rmiServer;
    private Registry registry;
    private String rmiService = "rmiMedicineManagementServer";

    public RMIMedicineManageControl(MedicineForm view) {
        this.view = view;
        view.addLoginListener(new RMIMedicineManageControl.SearchListener());
        try {
            // lay the dang ki
            registry = LocateRegistry.getRegistry(serverHost, serverPort);
            // tim kiem RMI server
            rmiServer = (RMISearchMedicineInterface) (registry.lookup(rmiService));
        } catch (RemoteException e) {
            view.showMessage(e.getStackTrace().toString());
            e.printStackTrace();
        } catch (NotBoundException e) {
            view.showMessage(e.getStackTrace().toString());
            e.printStackTrace();
        }
    }

    class SearchListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                String medicineName = view.getMedicineName();
                List<Medicine> listMedicine = rmiServer.searchMedicine(medicineName);
                System.out.println(listMedicine.get(0).toString());
                view.showListMedicine(listMedicine);
            } catch (Exception ex) {
                view.showMessage(ex.getStackTrace().toString());
                ex.printStackTrace();
            }
        }
    }
}
